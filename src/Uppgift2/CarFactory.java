package Uppgift2;

public class CarFactory{


    public ICar getMake (String make){
        if(make==null){
            return null;
        }
        if(make.equalsIgnoreCase("VOLVO")){
            return new Volvo();
        }
        else if(make.equalsIgnoreCase("BMW")){
            return new Bmw();
        }
        else if(make.equalsIgnoreCase("MERCEDES")){
            return new Mercedes();
        }
        return null;
    }
}
