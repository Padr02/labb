package Uppgift2;

public class Main {
    public static void main(String[] args) {

        //Factory-Design patter

        CarFactory carFactory = new CarFactory();

        ICar car1 = carFactory.getMake("Volvo");
        car1.make();

        ICar car2 = carFactory.getMake("bmw");
        car2.make();

        ICar car3 = carFactory.getMake("MERCEDES");
        car3.make();

    }
}
