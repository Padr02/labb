package Uppgift4;

public class Main implements Runnable{

    private final int start;
    private final int slut;

    public Main(int start, int slut) {
        this.start = start;
        this.slut = slut;
    }

    @Override
    public void run() {
        for (int i =start; i<slut; i++){
            if(isPrime(i)){
                System.out.println(Thread.currentThread().getName()+" : "+i);
            }
        }


    }

    static boolean isPrime (int n){
        if (n==1 || n==0) return false;

        for(int i =2; i <n; i++){
            if(n%i==0) return false;
        }
        return true;
    }

    public static void main(String[] args) {



        Runnable runnable1 = new Main(0,350000);
        Thread thread1 = new Thread(runnable1);
        thread1.start();

        Runnable runnable2 = new Main(350001,500000);
        Thread thread2 = new Thread(runnable2);
        thread2.start();







    }
}
