package Uppgift1;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {

        
        List<Person> person = List.of(
                new Person("John", "MALE", 29300.00),
                new Person("Sara", "FEMALE", 32500.00),
                new Person("Mike", "MALE", 28000.00),
                new Person("Jennifer", "FEMALE", 29000.00),
                new Person("Arnold", "MALE", 30700.00),
                new Person("Maria", "FEMALE", 29200.00),
                new Person("Chris", "MALE", 31100.00),
                new Person("Karen", "FEMALE", 36600.00),
                new Person("Bob", "MALE", 35500.00),
                new Person("Anna", "FEMALE", 23900.00)
        );

        //Uppger vilka key-pair values vi ska använda i vår map
        Map<String,Double> averageMaleAndFemale=person.stream()
                .collect(Collectors.groupingBy(Person::getGender,Collectors.averagingDouble(Person::getSalary)));
        System.out.println(averageMaleAndFemale);

        //Använder stream på ovan lista och samlar det som ska skrivas ut, sparas inte
        System.out.println(person.stream()
                .collect(Collectors.groupingBy(Person::getGender,Collectors.averagingDouble(Person::getSalary))));

        //Använder max för att ha ut högst lön med hjälp av Comparator interface som jämför dem olika värden,
        //sparas ej i något i detta fallet
        System.out.println(person.stream()
                .max(Comparator.comparing(Person::getSalary)));

        //Lägst lön i person listan
        System.out.println(person.stream()
                .min(Comparator.comparing(Person::getSalary)));


    }
}
