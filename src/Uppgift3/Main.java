package Uppgift3;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;


public class Main {


    public static void main(String[] args) {

        //Compile regex som predikat
        Predicate<String> filter = Pattern.compile("[aAeEiIoOuUyY][^ ]*[aAeEiIoOuUyY]")
                .asPredicate();

        List<String> list = List.of(
                "hello","world","my","name","is","Pavel","AE","perfect","strengths","month","fish"
        );

        List<String> filteredList = list.stream()
                .filter(filter)
                .toList();


        filteredList.forEach(System.out::println);

















    }
}
